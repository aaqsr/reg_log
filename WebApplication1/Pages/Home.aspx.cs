﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Users;
using System.Threading;

namespace WebApplication1
{
    public partial class Home : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // check if session state is not null
            // checks if user is logged in from the session state
            // redirects if not
            // reads the instance of the user class from session state 

            if (Auth.isUserLoggedIn(Session))
            {
                cUser user = (cUser)base.Session["user"];
                userName.Text = user.name;
            }
            else
            {
                //Response.Write(
                //    @"<script>
                //        alert('Not logged in. Redirecting to login page.');
                //    </script>"
                //);
                
                Thread.Sleep(3000);

                Response.Redirect("Auth/Login.aspx");
            }

        }

        // sets loggedIn in session to null and redirects to Login page
        protected void logOutButton_Click(object sender, EventArgs e)
        {
            Users.Auth.logOut(Session, Response);
        }

        protected void changePassButton_Click(object sender, EventArgs e) => Response.Redirect("Auth/Changepass.aspx");
    }
}