﻿<%@ Page Title="Book" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AddBook.aspx.cs" Inherits="WebApplication1.Pages.Books.AddBook" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <div class="body-content">
            <h1>Add:</h1>
        </div>
    <form id="form1" runat="server">
        <div>
           <table>
               <tr>
                   <td>Name: </td> <td style="width: 169px"><asp:TextBox ID="NameBox" runat="server"></asp:TextBox></td>
               </tr>
               <tr>
                   <td>Author: </td> <td style="width: 169px"><asp:TextBox ID="AuthBox" runat="server"></asp:TextBox></td>
               </tr>
               <tr>
                   <td>Description: </td> <td style="width: 169px"><asp:TextBox ID="DescBox" TextMode="multiline" runat="server" Height="67px"></asp:TextBox></td>
               </tr>
           </table>
        </div>
        <asp:Button ID="SubmitButton" runat="server" Height="43px" Text="Save and Add to Database" Width="224px" OnClick="SubmitButton_Click" />
        <div>
            <asp:Label ID="Label1" runat="server" style="color:darkred; font-style: italic; margin-top: 20px;"></asp:Label>
        </div>
    </form>
</asp:Content>
