﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Users;
using WebApplication1.Books;


namespace WebApplication1.Pages.Books
{
    public partial class AddBook : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Users.Auth.isUserLoggedIn(Session))
            {
                Response.Redirect("../Auth/Login.aspx");
            } 
        }

        protected void SubmitButton_Click(object sender, EventArgs e)
        {
            var NewBook = new Book()
            {
                Name = NameBox.Text,
                Author = AuthBox.Text,
                Description = DescBox.Text
            };

            try
            {
                Book.addBook(NewBook);
                Label1.Text = "Book Added";
            }
            catch
            {
                Label1.Text = "Err";
            }
        }
    }
}