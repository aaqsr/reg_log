﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="databaseview.aspx.cs" Inherits="WebApplication1.databaseview" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:Awab_TestDBConnectionString %>" SelectCommand="SELECT * FROM [userdata] ORDER BY [userId]"></asp:SqlDataSource>
        </div>
        <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="userId" DataSourceID="SqlDataSource1" Width="334px">
            <Columns>
                <asp:BoundField DataField="userId" HeaderText="userId" InsertVisible="False" ReadOnly="True" SortExpression="userId" />
                <asp:BoundField DataField="username" HeaderText="username" SortExpression="username" />
                <asp:BoundField DataField="pincode" HeaderText="pincode" SortExpression="pincode" />
            </Columns>
        </asp:GridView>
    </form>
</body>
</html>
