﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Users;

namespace WebApplication1.Pages.Auth
{
    public partial class Changepass : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // throws user to login if he is not logged in
            if (!Users.Auth.isUserLoggedIn(Session))
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void Button_Click1(object sender, EventArgs e)
        {
            cUser user = (cUser)Session["user"];
            string username = user.name;
            string oldPass = PassBox.Text;
            string newPass = NewPassBox.Text;

            if (Users.Auth.checkUser(user.name, oldPass))
            {
                if (Users.Auth.checkUser(user.name, newPass))
                {
                    Label1.Text = "New password can't be same as old password";
                }
                else
                {
                    try
                    {
                        Users.Auth.updateUserPass(user, newPass);
                        Users.Auth.logOut(Session, Response);
                    }
                    catch
                    {
                        Label1.Text = "Error: Unable to update password";
                    }
                }
            }
            else
            {
                Label1.Text = "Old Password does not match.";
            }
        }
    }
}