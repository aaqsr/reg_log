﻿<%@ Page  Title="Login"  MasterPageFile="~/Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="WebApplication1.Login" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
        <h1>Login: </h1>
<%--        <p>&nbsp;</p>
        User:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
        <asp:TextBox ID="TextBox1" runat="server"/>
&nbsp;<p>
            Password:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
        </p>--%>

        <table>
            <tr>
                <td>Username:</td>
                <td><asp:TextBox ID="UserBox" runat="server"/></td>
            </tr>
            <tr>
                <td>Password:</td>
                <td><asp:TextBox type="password" ID="PassBox" runat="server"/></td>
            </tr>

        </table>

        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Login" />
        <asp:Button ID="ForgotPass" runat="server" OnClick="ForgotPass_Click" Text="Forgot Password" />
        <div>
            <asp:Label ID="Label1" runat="server" style="color:darkred; font-style: italic; margin-top: 20px;"></asp:Label>
        </div>
    </form>
</asp:Content>
