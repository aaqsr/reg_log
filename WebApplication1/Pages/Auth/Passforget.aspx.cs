﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Users;

namespace WebApplication1.Pages.Auth
{
    public partial class passforget : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // logs out the user if they logged in 
            if (Users.Auth.isUserLoggedIn(Session))
            {
                Users.Auth.logOut(Session, Response);
            }
        }

        // checks if username is valid
        // checks if email is valid
        // generates code and sends it to the email
        // stores code in application state
        protected void Button_Click(object sender, EventArgs e)
        {
            string username = UserBox.Text;

            if (Users.Auth.checkUser(username))
            {
                // makes a random string and stores it in the app state with the user as its key
                // so that it can be later verified
                string rand = Users.Auth.genRand();

                Application[username] = rand;

                //debug code to just dump the application state into the label
#if DEBUG
                string appDump = "";
                foreach (string x in Application.Keys)
                {
                    appDump = appDump + x + ":" + Application[x] + "\n";
                }
                Label1.Text = appDump;
#endif

                string passResetLink = $"{HttpContext.Current.Request.Url.Scheme}://{HttpContext.Current.Request.Url.Authority}/Pages/Auth/passreset.aspx?username={username}&rand={rand}";

                Email.sendEmail($@"
                        Your reset link is {passResetLink} 

                        Don't share it.
                ", null); //null is temp value . in future send an email address from addressbox

                // since sendEmail was not implemented, for now it just sets it to the label.text
                Label1.Text = passResetLink;

            } 
            else
            {
                Label1.Text = "Username does not exist";
            }
        }
    }
}