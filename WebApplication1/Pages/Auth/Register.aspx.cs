﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Users;
using System.Threading;

namespace WebApplication1
{
    public partial class Register : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Auth.isUserLoggedIn(Session))
            {
                Response.Redirect("../Home.aspx");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string username = UserBox.Text;
            string password = PassBox1.Text;
            string rep_password = PassBox2.Text;

            // only execs if fields arent empty and password/repeat password are similar
            if ( (username != "") && (password != "") && (password == rep_password) )
            {

                Users.cUser newUser = Users.Auth.addUser(username, password);

                if (newUser != null)
                {
                    Label1.Text = "Registration Successful. Welcome " + newUser.name + ".\n Redirecting you to Home in 3 seconds.";

                    // stores in session state
                    Session["loggedIn"] = true;
                    Session["user"] = newUser;

                    // redirects to home page
                    Response.Redirect("../Home.aspx");
                }
                else
                {
                    Label1.Text = "User already exists";
                }
            } 
            else
            {
                Label1.Text = "Error, please re-enter details.";
            }
        }
    }
}