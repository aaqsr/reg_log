﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="passforget.aspx.cs" Inherits="WebApplication1.Pages.Auth.passforget" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
<h1>Forgot Password:</h1>

<form runat="server">

    <p>We will email you a link to reset your password if your username matches our records</p>
    <table>
        <tr>
            <td>Username:</td>
            <td><asp:TextBox runat="server" ID="UserBox" /></td>
        </tr>
<%--        <tr>
            <td>Email:</td>
            <td><asp:TextBox runat="server" ID="EmailBox" /></td>
        </tr>--%>
    </table>
    
    <div>
        <asp:Button ID="Button" runat="server" Text="Submit" OnClick="Button_Click" />
    </div>
    <div>
        <asp:Label ID="Label1" runat="server" style="color:darkred; font-style: italic; margin-top: 20px;"></asp:Label>
    </div>
</form>

</asp:Content>
