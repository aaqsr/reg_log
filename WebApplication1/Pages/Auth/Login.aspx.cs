﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Threading;
using WebApplication1.Users;

namespace WebApplication1
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ViewState["Heading"] = "Welcome Old User";
            if (Users.Auth.isUserLoggedIn(Session))
            {
                Response.Redirect("../Home.aspx");
            }
        }
        protected void Button1_Click(object sender, EventArgs e)
        {

            // Read the labels
            string username = UserBox.Text;
            string password = PassBox.Text;

            // makes sure fields arent blank
            if ((username != "") && (password != ""))
            {
                // tries to log in by explicitly checking the username password pair
                // also passes a reference variable error that contains an error message if login fails
                string error = "";
                bool login_success = Users.Auth.checkUser(username, password, ref error);

                if (login_success)
                {
                    // creates instance of the Users class. Redirects user to home page. Passes the newUser object to it 
                    Users.cUser newUser = new Users.cUser(username);
                    Label1.Text = "Welcome " + newUser.name + "\n Redirecting you in 3 seconds";

                    // stores in session state
                    Session["loggedIn"] = true;
                    Session["user"] = newUser;

                    // redirects to home page
                    Response.Redirect("../Home.aspx");
                }
                else
                {
                    // note: the way this is implemented wherein it shows when the username matches but the pass doesn't
                    // is insecure. This is for demonstration purposes only.
                    Label1.Text = error;
                }
            } 
            else
            {
                Label1.Text = "Please enter Username and Password";
            }
        }

        protected void ForgotPass_Click(object sender, EventArgs e)
        {
            Response.Redirect("Passforget.aspx");
        }
    }
}