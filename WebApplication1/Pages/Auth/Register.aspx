﻿<%@ Page  Title="Register"  MasterPageFile="~/Site.Master" Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="WebApplication1.Register" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        function TextChanged()
        {
            let username = document.getElementById("MainContent_UserBox");
            let password = document.getElementById("MainContent_PassBox1");
            let confirm = document.getElementById("MainContent_PassBox2");
            let message = document.getElementById("MainContent_Label1");

            if (username.value !== "")
            {
                if ((password.value !== "") && (password.value === confirm.value))
                {
                    message.innerHTML = "Passwords Match!";
                }
                else
                {
                    message.innerHTML = "Passwords Do Not Match!";
                }
            }
            else
            {
                message.innerHTML = "Username cannot be blank.";
            }
        }
        
    </script>
    <form id="form1" runat="server">
        <h1>Register: </h1>

        <table>
            <tr>
                <td>Username:</td>
                <td><asp:TextBox ID="UserBox" runat="server"/></td>
            </tr>
            <tr>
                <td>Password:</td>
                <td><asp:TextBox type="password" ID="PassBox1" runat="server" OnChange="TextChanged()"/></td>
            </tr>
            <tr>
                <td>Repeat Password:   </td>
                <td><asp:TextBox type="password" ID="PassBox2" runat="server" OnChange="TextChanged()"/></td>
            </tr>
        </table>

        <div>
            <asp:Button ID="Button1" runat="server" Text="Register" OnClick="Button1_Click"/>
        </div>
            <asp:Label ID="Label1" runat="server" style="color:darkred; font-style: italic; margin-top: 20px;"></asp:Label>
    </form>
</asp:Content>
