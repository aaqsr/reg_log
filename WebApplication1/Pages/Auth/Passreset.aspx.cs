﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication1.Users;

namespace WebApplication1.Pages.Auth
{
    public partial class passreset : System.Web.UI.Page
    {
        private cUser user { get; set; }
        private string rand { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            // logs out the user if they logged in 
            if (Users.Auth.isUserLoggedIn(Session))
            {
                Users.Auth.logOut(Session, Response);
            }


            // checks if query string is correct, ortherwise redirects back to passforget page
            try
            {
                user = new cUser(Request.QueryString["username"]);
                rand = Request.QueryString["rand"];

                if ((string)Application[user.name] != rand)
                {
                    Response.Redirect("Passforget.aspx");
                }
            }
            catch
            {
                Response.Redirect("Passforget.aspx");
            }
            
        }

        protected void Button_Click1(object sender, EventArgs e)
        {
            string newPass = PassBox.Text;

            if (Users.Auth.checkUser(user.name, newPass))
            {
                Label1.Text = "New password can't be the same as old password";
            }
            else
            {
                try
                {
                    Users.Auth.updateUserPass(user, newPass);
                    Response.Redirect("Login.aspx");
                }
                catch
                {
                    Label1.Text = "Error: Unable to update password";
                }
            }
        }
    }
}