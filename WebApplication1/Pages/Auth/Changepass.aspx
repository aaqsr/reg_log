﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Changepass.aspx.cs" Inherits="WebApplication1.Pages.Auth.Changepass" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Change Password:</h1>
 <form runat="server">
        <table>
            <tr>
                <td>Old Password:</td>
                <td><asp:TextBox type="password" runat="server" ID="PassBox" /></td>
            </tr>
            <tr>
                <td>New Password:</td>
                <td><asp:TextBox type="password" runat="server" ID="NewPassBox" /></td>
            </tr>
        </table>
        <div>
            <asp:Button ID="Button" runat="server" Text="Submit" OnClick="Button_Click1" />
        </div>
        <div>
            <asp:Label ID="Label1" runat="server" style="color:darkred; font-style: italic; margin-top: 20px;"></asp:Label>
        </div>
    </form>
</asp:Content>
