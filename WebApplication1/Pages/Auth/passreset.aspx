﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="passreset.aspx.cs" Inherits="WebApplication1.Pages.Auth.passreset" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Reset Password:</h1>

    <p>Please enter a new password in the field below. Note that old password can not be the same as the new one.</p>

    <form runat="server">
        <table>
            <tr>
                <td>New Password:</td>
                <td><asp:TextBox type="password" runat="server" ID="PassBox" /></td>
            </tr>
        </table>
        <div>
            <asp:Button ID="Button" runat="server" Text="Submit" OnClick="Button_Click1" />
        </div>
        <div>
            <asp:Label ID="Label1" runat="server" style="color:darkred; font-style: italic; margin-top: 20px;"></asp:Label>
        </div>
    </form>
</asp:Content>
