﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Home.aspx.cs" Inherits="WebApplication1.Home" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <form id="form1" runat="server">
    <div class="body-content">
        Welcome to home <asp:Label ID="userName" runat="server"/>
    </div>
        <div>
            <asp:Button ID="logOut" runat="server" OnClick="logOutButton_Click" Text="Log Out" />
            <asp:Button ID="changePassButton" runat="server" OnClick="changePassButton_Click" Text="Change Password" />
        </div>
    </form>
</asp:Content>