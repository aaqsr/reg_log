﻿using System;
using System.Configuration;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Security.Cryptography;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using System.Reflection;

namespace WebApplication1.Database
{

    // Holds methods for database connection
    // refrain from calling this class from outside the namespace
    // todo: maybe shift this from an abstract class to an instantiated class? probs not
    abstract class Connection
    {  
        private static string connectionString = configParser();

        // parses the DatabaseConfig.json file and returns the database connection string which is store on the class
        private static string configParser()
        {
            //string con_str = ConfigurationManager.ConnectionStrings["centegy"].ConnectionString;

            //Debug.WriteLine(con_str);
            //return con_str;

            return @"Data Source = 128.1.112.1; Initial Catalog = Awab_TestDB; User ID = SNDPRO; Password = national";
        }

        // connects to the database
        public static SqlConnection databaseConnect()
        {
            var con = new SqlConnection(connectionString);

            // try connecting
            try
            {
                con.Open();
                return con;
            }
            catch
            {
                //implement this later
                throw new Exception("Failure to connect to database.");
            }
        }

        // disconnects database
        public static void databaseDisconnect(SqlConnection con)
        {
            con.Close();
        }
    }

}