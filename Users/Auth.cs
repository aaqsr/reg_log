﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Database.UsersData;

namespace WebApplication1.Users
{
    // Authenticates the users by calling the database classes
    public abstract class Auth
    {
        // checks if username is in the database if string is not empty
        public static bool checkUser(string username) => string.IsNullOrWhiteSpace(username) ? false : UserAuthentication.checkUserInDatabase(username);

        // overloads the previous method and checks if username and pass are in database if string is not empty
        public static bool checkUser(string username, string pass) => string.IsNullOrWhiteSpace(username) ? false : UserAuthentication.checkUserInDatabase(username, pass);
        public static bool checkUser(string username, string pass, ref string err)
        {
            if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(pass))
            {
                err = "Fields cannot be empty";
                return false;
            }
            else
            {
                if (Auth.checkUser(username))
                {
                    if (UserAuthentication.checkUserInDatabase(username, pass))
                    {
                        return true;
                    }
                    else
                    {
                        err = "Password does not match";
                        return false;
                    }
                }
                else
                {
                    err = "Username does not match";
                    return false;
                }
            }
        }

        // adds a user. Returns new user when successful. Returns null when fail
        public static cUser addUser(string username, string password)
        {
            // first check if username already exists 
            if (checkUser(username))
            {
                UserAuthentication.addUserInDatabase(username, password);
                return new cUser(username);
            }
            else
            {
                return null;
            }
        }

        // logs out the user and redirects to the login page 
        public static void logOut (System.Web.SessionState.HttpSessionState Session, HttpResponse Response)
        {
            Session["loggedIn"] = null;
            Session["user"] = null;
            //Response.Redirect("Auth/Login.aspx");
            Response.Redirect($"{HttpContext.Current.Request.Url.Scheme}://{HttpContext.Current.Request.Url.Authority}/Pages/Auth/Login.aspx");

        }

        // checks if the user is logged in by reading the session state's "loggedIn" key
        // doesnt actually know which use is logged in
        public static bool isUserLoggedIn(System.Web.SessionState.HttpSessionState Session)
        {
            if (Session["loggedIn"] != null)
            {
                if ((bool)Session["loggedIn"] == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        // returns random string
        public static string genRand() => Database.Crypt.genRand();

        // updates the user's password
        public static void updateUserPass(cUser user, string newPass) => UserAuthentication.updateUserPass(user.name, newPass);
        
    }

}