﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication1.Database.UsersData;

namespace WebApplication1.Users
{
    // Calls the database classes to read the database to create a User object 
    public class cUser
    {
        public string name { get; }

        public cUser(string username)
        {
            if (UserAuthentication.checkUserInDatabase(username))
            { // is this really necessary? We call this twice in most places
                this.name = username;
            }
            else
            {
                throw new Exception("User not in database");
            }
        }
    }
}