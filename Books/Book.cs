﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebApplication1.Books
{
    public partial class Book
    {
        public string Name { get; set; }
        public string Author { get; set; }
        public string Description { get; set; }
    }
}
